<?php
parse_str($argv[1], $params);
if($params['id'] === 'CHANGE_THIS_TO_SOMETHING_RANDOM') {
    define('permit_incl', TRUE);
    include('db.php');

    $sql="SELECT id, when_to_delete FROM data";
    $result = $conn->query($sql);
    $time = new DateTime();
    $ids_to_delete = array();

    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $delete_date = $row["when_to_delete"];
            $id = $row["id"];
            if($time > new DateTime($delete_date)) {
                if(strpos( $id, '.' ) !== false) {
                    array_push($ids_to_delete, $id);
                    unlink('/var/www/upload/i/'.$id.'');
                } else {
                    array_push($ids_to_delete, $id);
                    unlink('/var/www/upload/ie/'.$id.'.aes');
                }
            }
        }
    } 

    if (!empty($ids_to_delete)) {
        $string_ids = "'".implode("','", $ids_to_delete)."'";
        $sql = "UPDATE data SET exists_on_the_disk=0 WHERE id IN (".$string_ids.")";

        if ($conn->query($sql) !== TRUE) {
           die();
        }
    } 
    $conn->close();
}
?>
