<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="theme-color" content="#a20000">
    <meta name="msapplication-navbutton-color" content="#a20000">
    <meta name="apple-mobile-web-app-status-bar-style" content="#a20000">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="robots" content="noindex">
    <link rel="icon" href="https://vaa.red/fav.png" sizes="96x96">
    <style type="text/css">
    /* thanks for the anonymous one who contributed to these styles! https://www.reddit.com/r/uploadvaared/comments/a29m05/a_couple_of_questions/ */
    html {
        margin: 0;
        padding: 0;
        color: #fff;
        background-color: #0e0e0e;
        text-align: center;
    }

    body {
        font-size: 5rem;
        font-family: sans-serif;
        font-weight: bold;
    }
    </style>
    <title>Delete file – upload.vaa.red</title>
</head>
<body>
<?php
if(isset($_GET['id'])) {
    if(preg_match("/^[a-zA-Z0-9]+$/", $_GET['id']) != 1) {
        echo "upload deleted!"; exit();
    }
    define('permit_incl', TRUE);
    include('../db.php');
    $id = mysqli_real_escape_string($conn, $_GET['id']);

    $sql="SELECT id FROM data WHERE deletion_url = '".$id."' AND exists_on_the_disk=1";
    $found = false;
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $uploadId = $row["id"];
            $found = true;
            break;
        }
    }

    if($found) {
        $sql = "UPDATE data SET exists_on_the_disk=0 WHERE deletion_url='".$id."'";

        if ($conn->query($sql) === TRUE) {
            if(file_exists('/var/www/upload/ie/'.$uploadId.'.aes')) {
                unlink('/var/www/upload/ie/'.$uploadId.'.aes');
            } else {
                unlink('/var/www/upload/i/'.$uploadId.'');
            }
            echo "upload deleted!";
        } else {
            echo "upload deleted!";
        }
    } else {
        echo "upload deleted!";
    }
    $conn->close();
} else {
    echo 'invalid id'; exit();
}
?>
</body>
</html>


