## Guide to self-host with LAMP installation

**In this guide we are assuming that your site will running on URL: `https://example.com/upload-vaa`.
Change directories and URLs accordingly during the steps.** There's not yet any simple and easy installation file.

### 1. Get all the files
Clone the source repository:

```
git clone https://gitlab.com/vaared/upload.vaa.red.git
```

Copy the `src` files to your www-directory. For example:

```
cp -R upload.vaa.red/src/* /var/www/html/upload-vaa/
```
Also make sure that you have proper file permissions to the wwww-directory (if some command gives permission error, try using `sudo`).

### 2. Create database and tables
```
cd /var/www/html/upload-vaa/
```

Run `setup.sql` to your mysql.

For example: through command line: 
```
mysql -u root -p < setup.sql
```
or through phpMyAdmin.

### 3. Apache configurations
For example:
```
sudo nano /etc/apache2/sites-available/000-default-le-ssl.conf
```

Add these configs:

```
<Directory /var/www/html/upload-vaa>
    Options -Indexes
</Directory>
RewriteEngine On
RewriteRule ^/upload-vaa/([0-9A-Za-z]+)$ /upload-vaa/index.php?id=$1 [L]
RewriteRule ^/upload-vaa/delete/([0-9A-Za-z]+)$ /upload-vaa/delete/index.php?id=$1 [L]
RewriteRule ^/upload-vaa/upload/file /upload-vaa/uploader.php
```

### 4. Replace default URLs and directories
Make sure you are in your www-directory (`cd /var/www/html/upload-vaa/`).

Edit `WWW_DIR` to your www-directory and run this command on your terminal (**no trailing slash**):
```
WWW_DIR=/var/www/html/upload-vaa
```
And then run this command (again, make sure your current path is your www-directory `cd /var/www/html/upload-vaa/`):
```
find ./ -type f -exec sed -i "s|/var/www/upload|$WWW_DIR|g"  {} \;
```
Edit `WWW_URL` to your URL (**no trailing slash or _https://_**):
```
WWW_URL=example.com/upload-vaa
```
And then run this command:
```
find ./ -type f -exec sed -i "s|upload.vaa.red|$WWW_URL|g"  {} \;
```


### 5. Generate all the CSS/JS/HTML files
```
sudo npm install -g cssnano \
sudo npm install -g cssnano-cli \
sudo npm install -g npm \
sudo npm install -g html-minifier \
sudo npm install -g uglify-js
```
```
cd js/original
```
```
cat jquery.js crypto.js main.js | uglifyjs --compress --mangle -o app.js
```
```
cat jquery.js crypto.js decrypt.js | uglifyjs --compress --mangle -o app.decrypt.js
```
```
mv app.js app.decrypt.js ../
```
```
cd ../../
```
```
cd css/original
```
```
cssnano main.css main.min.css
```
```
cssnano decrypt.css decrypt.min.css
```
```
mv main.min.css decrypt.min.css ../
```
```
cd ../../
```
```
html-minifier --collapse-whitespace --remove-comments --remove-optional-tags --remove-redundant-attributes --remove-script-type-attributes front_template.html -o front_template_minified.html
```

### 6. Permissions
```
sudo chown -R www-data:www-data ie/
```
```
sudo chown -R www-data:www-data i/
```
```
sudo chmod 640 db.php
```

### 7. Database credentials
Edit the file and use your mysql credentials.
```
sudo nano db.php
```

### 8. Automatic file deletion
```
sudo crontab -e
```
Add this to the end of the file, but edit the `CHANGE_THIS_TO_SOMETHING_RANDOM` to random string:
```
* * * * * php /var/www/html/upload-vaa/auto_delete.php id=CHANGE_THIS_TO_SOMETHING_RANDOM > /dev/null 2>&1
```
Edit the `auto_delete.php` file to use the same random string:
```
sudo nano auto_delete.php
```
### 9. php.ini configs
Add these configs to your `php.ini` file. For example:

```
sudo nano /etc/php/7.0/apache2/php.ini
```
Find these configs and change them to 50M:
```
upload_max_filesize = 50M
post_max_size = 50M
```
Note: the default max filesize for the encrypted uploads are 30 MB, but because of the client-side encryption, 30 MB files will be about 45-49 MB after encryption. That's why the 50 MB limit. Max filesize isn't yet configurable via any config file, but if you want to change it now you can edit the `uploader.php` and `main.js` files.

### 10. Remove installation files and restart apache
```
rm setup.sql
```
```
sudo service apache2 restart
```

Now the site should be live `https://example.com/upload-vaa`.
